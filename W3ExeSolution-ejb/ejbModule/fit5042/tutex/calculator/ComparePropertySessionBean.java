package fit5042.tutex.calculator;

import javax.ejb.CreateException;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import fit5042.tutex.repository.entities.Property;
@Stateful

// a stateful implemention class with three methods which are declared in the interface
public class ComparePropertySessionBean implements CompareProperty {
    private final List<Property> propertyList;
	public ComparePropertySessionBean() {
        this.propertyList = new ArrayList<>();
    }
	
	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
            this.propertyList.add(property);

	}

	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
        this.propertyList.remove(property);
	}

	@Override
	public int getBestPerRoom() {
		// TODO Auto-generated method stub
		int chosenID = 0;
		int minAvgPrice = 9999;
        for (Property property : propertyList) {
        	int avgPrice;
        	avgPrice = (int) (property.getPrice() / property.getNumberOfBedrooms());
            if (avgPrice <= minAvgPrice) {
                minAvgPrice = avgPrice;
            	chosenID = property.getPropertyId();
                }
        }
		return chosenID;
	}




}
