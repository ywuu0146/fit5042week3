package fit5042.tutex.calculator;

import javax.ejb.Remote;
import javax.ejb.Stateful;
import fit5042.tutex.repository.entities.Property;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

// a remote interface with three required methods
@Remote
public interface CompareProperty {
	void addProperty(Property property);
	void removeProperty(Property property);
	int getBestPerRoom();



    
}
